import { Meteor } from 'meteor/meteor'

Accounts.onLogin(() => {
  Meteor.logoutOtherClients();
  Session.set('loggedIn', true);
});
