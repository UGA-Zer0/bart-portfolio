import { Meteor } from 'meteor/meteor'

Meteor.startup(() => {
  Tracker.autorun(() => {
    if (!Meteor.userId()) {
      if (Session.get('loggedIn')) {
        let currentRoute = FlowRouter.current();
        Session.set('loginRedirect', currentRoute.path);
        FlowRouter.go(FlowRouter.path('public.login'));
      }
    }
  })
});
