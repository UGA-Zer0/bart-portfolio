'use strict';

import {Meteor} from 'meteor/meteor';
import {Accounts} from 'meteor/accounts-base';
import {check, Match} from 'meteor/check';

Meteor.startup(() => {
  console.log("--- --- --- --- ---");
  console.log("   Starting app..  ");
  console.log("--- --- --- --- ---");

  // user allow/deny rules
  Meteor.users.allow({
    insert() { return false; },
    update() { return false; },
    remove() { return false; }
  });

  Meteor.users.deny({
    insert() { return true; },
    update() { return true; },
    remove() { return true; }
  });

  // check if user collection is empty
  if (Meteor.users.find().count() === 0) {
    console.log("Users collection is empty - creating default account");

    // set default user details
    let default_user = {
      username: "Admn",
      email: "contact@alextemple.io",
      password: "changeme"
    }

    Accounts.createUser(default_user);
    console.log("Default user account created");
  }

  
});
