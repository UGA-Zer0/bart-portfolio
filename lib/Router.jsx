import React from 'react';
import {Meteor} from 'meteor/meteor';
import {mount} from 'react-mounter';

// import main app layout
import App from '../imports/ui/App.jsx';

// import individual views
import HomeContent from '../imports/ui/views/Home.jsx';
import PiecesContent from '../imports/ui/views/Pieces.jsx';
import ContactContent from '../imports/ui/views/Contact.jsx';
import AboutContent from '../imports/ui/views/About.jsx';
import LoginContent from '../imports/ui/views/Login.jsx';
import NotFoundContent from '../imports/ui/views/NotFound.jsx';

// set up flow Router
FlowRouter.notFound = {
  action: () => {
    mount(App, {
      content: <NotFoundContent />
    });
  }
}

// FlowRouter groups
let _public = FlowRouter.group();
let _restricted = FlowRouter.group({
  prefix: '/admin',
  name: 'admin',
  triggersEnter: [(context, redir) => {
    if (!Meteor.loggingIn && !Meteor.userId) {
      let route = FlowRouter.current();
      if (route.route.name != 'public.login') {
        Session.set('loginRedirect', route.path);
      }
      FlowRouter.go('public.login');
    }
  }]
});

// public routes
_public.route("/", {
  name: 'public.projects',
  action() {
    mount(App, {
      content: <HomeContent />
    });
  }
});

_public.route("/pieces", {
  name: 'public.pieces',
  action() {
    mount(App, {
      content: <PiecesContent />
    });
  }
});

_public.route("/contact", {
  name: 'public.contact',
  action() {
    mount(App, {
      content: <ContactContent />
    });
  }
});

_public.route("/about", {
  name: 'public.about',
  action() {
    mount(App, {
      content: <AboutContent />
    });
  }
});

_public.route("/login", {
  name: 'public.login',
  action() {
    mount(App, {
      content: <LoginContent />
  });
  }
})

// restricted routes
_restricted.route("/", {
  name: 'admin.home',
  action() {
    mount(App, {
      content: <LoginContent />
    })
  }
})
