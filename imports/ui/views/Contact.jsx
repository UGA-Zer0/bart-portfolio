import React, {Component} from 'react';

export default class ContactContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: false
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(ev) {
    ev.preventDefault();

    this.setState({
      errors: "TEST ERROR"
    });
  }

  renderErrors() {
    if (this.state.errors != false) {
      return (
        <div className="form-errors">
          {this.state.errors}
        </div>
      )
    }
    else {
      return null;
    }
  }

  render() {
    return (
      <div className="right nomargin" id="content">
        <h2>Contact</h2>

      <form className="form" onSubmit={this.handleSubmit}>

        {this.renderErrors()}

        <label htmlFor="name">
          <span>Name</span>
          <input type="text" name="name" className="form-input text" />
        </label>

        <label htmlFor="email">
          <span>Email</span>
          <input type="text" name="email" className="form-input text" />
        </label>

        <label htmlFor="message">
          <span>Message</span>
          <textarea rows="10" name="message" className="form-input textarea" />
        </label>

        <br /><br />

        <label htmlFor="submit">
          <span>&nbsp;</span>
          <input type="submit" name="submit" className="form-button" value="Send" />
        </label>

      </form>

      </div>
    )
  }
}
