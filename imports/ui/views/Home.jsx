import React, {Component} from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import _ from 'lodash';

export default class HomeContent extends Component {

  render() {
    return (
      <div className="right nomargin" id="content">
        <ReactCSSTransitionGroup transitionName="fade-anim" transitionAppear={true} transitionAppearTimeout={700} transitionEnterTimeout={700} transitionLeaveTimeout={700} component="div" className="flex-container">
          {_.map(_.fill(new Array(9), 1), (el, i) => {
            return <div className="project" key={i}>&nbsp;</div>;
          })}
        </ReactCSSTransitionGroup>
      </div>
    )
  }

}
