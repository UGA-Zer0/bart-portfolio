import React, {Component} from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {Meteor} from 'meteor/meteor';
import _ from 'lodash';

export default class LoginContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: false
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(ev) {
    ev.preventDefault();

    let email = this.refs.email.value;
    let password = this.refs.password.value;

    if (email !== "" && password !== "") {
      Meteor.loginWithPassword({email: email}, password, (err) => {
        if (err) {
          this.setState({
            errors: err.reason
          });
        }
        else {
          let redirect = Session.get('loginRedirect');
          if (redirect && redirect != '/login') {
            FlowRouter.go(redirect);
          }
        }
      });
    }
  }

  renderErrors() {
    if (this.state.errors != false) {
      return (
        <ReactCSSTransitionGroup transitionName="fade-anim" transitionAppear={true} transitionAppearTimeout={700} transitionEnterTimeout={700} transitionLeaveTimeout={700} component="div" className="flex-container">
          <div className="form-errors">
            {this.state.errors}
          </div>
        </ReactCSSTransitionGroup>
      )
    }
    else {
      return null;
    }
  }

  render() {
    return (
      <div className="right nomargin" id="content">
        <h2>Login</h2>

        <form className="form" onSubmit={this.handleSubmit}>
          {this.renderErrors()}

          <label htmlFor="email">
            <span>Email</span>
            <input type="email" ref="email" className="form-input text" />
          </label>

          <label htmlFor="password">
            <span>Password</span>
            <input type="password" ref="password" className="form-input text" />
          </label>

          <br /><br />

          <label htmlFor="submit">
            <span>&nbsp;</span>
            <input type="submit" className="form-button" value="Send" />
          </label>

        </form>
      </div>
    )
  }

}
