import React, {Component} from 'react';

export default class NotFoundContent extends Component {

  render() {
    return (
      <div className="right nomargin" id="content">
        <h1>404</h1>
        <h3>The page you are looking for was not found.</h3>
      </div>
    )
  }

}
