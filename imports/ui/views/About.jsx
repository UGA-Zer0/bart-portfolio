import React, {Component} from 'react';
import lipsum from 'lorem-ipsum';

export default class AboutContent extends Component {
  render() {
    return (
      <div className="right nomargin" id="content">
        <h2>About</h2>

        <div className="image-right">

        </div>

        {Array(4).fill(1).map((el, i) =>
          <p key={i}>{lipsum({units: 'paragraphs'})}</p>
        )}
      </div>
    )
  }
}
