import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {createContainer} from 'meteor/react-meteor-data';

class App extends Component {
  constructor(props) {
    super(props);
  }
  renderUserBar() {
    if (this.props.loggedIn) {
      return (
        <div className="user-bar">
          <p>Logged in as {this.props.currentUser.username}.</p>
          <a href="" onClick={this.handleLogout}>Logout</a>
        </div>
      )
    }
    else {
      return null;
    }
  }

  handleLogout(ev) {
    ev.preventDefault();
    Meteor.logout();
  }

  render() {
    return (
      <div id="wrapper">
          {this.renderUserBar()}
          <div className="left nomargin" id="sidebar">

            <div className="nomargin nopad">
              <img className="header-image" src="/img/banner.png"></img>
            </div>

            <ul className="sidebar">
              <li><a className={ActiveRoute.name('public.projects') ? 'active' : ''} href={FlowRouter.path('public.projects')}>projects.</a></li>
              <li><a className={ActiveRoute.name('public.pieces') ? 'active' : ''} href={FlowRouter.path('public.pieces')}>pieces.</a></li>
              <li><a className={ActiveRoute.name('public.contact') ? 'active' : ''} href={FlowRouter.path('public.contact')}>contact.</a></li>
              <li><a className={ActiveRoute.name('public.about') ? 'active' : ''} href={FlowRouter.path('public.about')}>about.</a></li>
            </ul>
          </div>

          {this.props.content}

          <div id="footer">
            <span>Copyright &copy; 2017 Bart Turner All Rights Reserved. {this.props.loggedIn && !this.props.loggingIn ? null : <a href={FlowRouter.path('public.login')}>Log in</a>}</span>
          </div>

      </div>
    )
  }
}

App.propTypes = {
  loggedIn: PropTypes.bool.isRequired
};

export default createContainer(() => {
  return {
    loggedIn: !!Meteor.user(),
    currentUser: Meteor.user() || null,
    loggingIn: !!Meteor.loggingIn()
  };
}, App)
