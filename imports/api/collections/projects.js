import {Mongo} from 'meteor/mongo';

const Projects = new Mongo.Collection('projects');

Projects.allow({
  insert() { return false; },
  update() { return false; },
  remove() { return false; }
});

Projects.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; }
});
